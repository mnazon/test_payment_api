<?php

declare(strict_types=1);

namespace App\Api\Exception;

use App\Api\DTO\Http\Response\ApiError;
use Throwable;

class ApiErrorException extends \Exception implements ApiErrorExceptionInterface
{
    private $apiError;
    private $statusCode;
    private $headers;

    public function __construct(
        ApiError $apiError,
        int $statusCode = 400,
        array $headers = [],
        string $message = '',
        int $code = 0,
        Throwable $previous = null
    ) {
        parent::__construct($message, $code, $previous);
        $this->statusCode = $statusCode;
        $this->headers = $headers;
        $this->apiError = $apiError;
    }

    public function getApiError(): ApiError
    {
        return $this->apiError;
    }

    public function getStatusCode(): int
    {
        return $this->statusCode;
    }

    public function getHeaders(): array
    {
        return $this->headers;
    }
}
