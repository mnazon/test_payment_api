<?php

declare(strict_types=1);

namespace App\Api\Exception;

use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;
use Symfony\Component\Validator\ConstraintViolationListInterface;

class ConstraintViolationListException extends \Exception implements HttpExceptionInterface
{
    private $constraintViolationList;
    private $statusCode;
    private $headers;

    public function __construct(
        ConstraintViolationListInterface $constraintViolationList,
        int $statusCode = 400,
        array $headers = []
    ) {
        $this->constraintViolationList = $constraintViolationList;

        parent::__construct('validation errors');
        $this->statusCode = $statusCode;
        $this->headers = $headers;
    }

    public function getConstraintViolationList(): ConstraintViolationListInterface
    {
        return $this->constraintViolationList;
    }

    public function getStatusCode(): int
    {
        return $this->statusCode;
    }

    public function getHeaders(): array
    {
        return $this->headers;
    }
}
