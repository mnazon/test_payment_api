<?php

declare(strict_types=1);

namespace App\Api\Exception;

use App\Api\DTO\Http\Response\ApiError;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;

interface ApiErrorExceptionInterface extends HttpExceptionInterface
{
    public function getApiError(): ApiError;
}
