<?php

declare(strict_types=1);

namespace App\Api\Service;

use App\Api\DTO\Http\PaymentRequest;
use App\Api\DTO\Http\Response\ApiError;
use App\Api\Entity\PaymentLog;
use App\Api\Entity\User;
use App\Api\Enum\PaymentType;
use App\Api\Exception\ApiErrorException;
use App\Api\Repository\PaymentLogRepository;
use App\Api\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Lock\LockFactory;
use Symfony\Component\Lock\PersistingStoreInterface;
use Symfony\Component\Lock\Store\RetryTillSaveStore;

class PaymentManager
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;
    /**
     * @var UserRepository
     */
    private $userRepository;
    /**
     * @var PaymentLogRepository
     */
    private $paymentLogRepository;
    /**
     * @var PersistingStoreInterface
     */
    private $redisStore;
    /**
     * @var LockFactory
     */
    private $lockFactory;

    public function __construct(
        EntityManagerInterface $entityManager,
        UserRepository $userRepository,
        PaymentLogRepository $paymentLogRepository,
        PersistingStoreInterface $redisStore
    ) {
        $this->entityManager = $entityManager;
        $this->userRepository = $userRepository;
        $this->paymentLogRepository = $paymentLogRepository;
        $this->redisStore = new RetryTillSaveStore($redisStore);
        $this->lockFactory = new LockFactory($this->redisStore);
    }

    /**
     * @param PaymentRequest $payment
     *
     * @throws ApiErrorException when wrong uid.
     */
    public function credit(PaymentRequest $payment): void
    {
        $this->createLockAndDoPayment(PaymentType::CREDIT(), $payment);
    }

    /**
     * @param PaymentRequest $payment
     *
     * @throws ApiErrorException when insufficient funds or wrong uid.
     */
    public function debit(PaymentRequest $payment): void
    {
        $this->createLockAndDoPayment(PaymentType::DEBIT(), $payment);
    }

    /**
     * @param PaymentType    $paymentType
     * @param PaymentRequest $payment
     *
     * @throws ApiErrorException
     */
    private function createLockAndDoPayment(PaymentType $paymentType, PaymentRequest $payment): void
    {
        $lockKeyByUserId = sprintf('payment-%s', $payment->getUid());
        $lock = $this->lockFactory->createLock($lockKeyByUserId, 3);
        $lock->acquire(true);
        try {
            $existingPaymentLog = $this->paymentLogRepository->findOneByTransactionId($payment->getTid());
            if (!$existingPaymentLog) {
                $user = $this->findUser($payment->getUid());
                $this->doPaymentByUser($user, $paymentType, $payment);
            }
        } finally {
            $lock->release();
        }
    }

    /**
     * @param User           $user
     * @param PaymentType    $paymentType
     * @param PaymentRequest $payment
     *
     * @throws ApiErrorException
     */
    private function doPaymentByUser(User $user, PaymentType $paymentType, PaymentRequest $payment): void
    {
        $wallet = $user->getWallet();
        if ($paymentType->equals(PaymentType::DEBIT())) {
            if ($wallet->getBalance() < $payment->getAmount()) {
                $apiError = new ApiError('insufficient funds.');
                throw new ApiErrorException($apiError);
            }

            $newBalance = $wallet->getBalance() - $payment->getAmount();
        } else {
            $newBalance = $wallet->getBalance() + $payment->getAmount();
        }

        $wallet->setBalance($newBalance);

        $log = new PaymentLog(
            $user,
            $paymentType,
            $payment->getAmount(),
            $payment->getTid()
        );

        $this->entityManager->persist($log);
        $this->entityManager->flush();
    }

    /**
     * @param string $uid
     *
     * @return User
     * @throws ApiErrorException
     */
    private function findUser(string $uid): User
    {
        /** @var User $user */
        $user = $this->userRepository->find($uid);
        if (!$user) {
            $apiError = new ApiError('wrong uid.');
            throw new ApiErrorException($apiError);
        }

        return $user;
    }
}
