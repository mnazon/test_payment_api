<?php

declare(strict_types=1);


namespace App\Api\Service\Serializer;


use App\Api\DTO\Http\PaymentRequest;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;

class PaymentNormalizer implements DenormalizerInterface
{
    public function denormalize($data, string $type, string $format = null, array $context = []): PaymentRequest
    {
        return new PaymentRequest(
            isset($data['@amount']) && is_numeric($data['@amount']) ? (int) $data['@amount'] : null,
            $data['@tid'] ?? null,
            $data['@uid'] ?? null
        );
    }

    public function supportsDenormalization($data, string $type, string $format = null): bool
    {
        return PaymentRequest::class === $type;
    }
}
