<?php

declare(strict_types=1);

namespace App\Api\Service\Serializer;

use Symfony\Component\Serializer\Normalizer\ConstraintViolationListNormalizer as CoreConstraintViolationListNormalizer;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Validator\ConstraintViolationListInterface;

class ConstraintViolationListNormalizer implements NormalizerInterface
{
    private $normalizer;

    public function __construct(CoreConstraintViolationListNormalizer $normalizer)
    {
        $this->normalizer = $normalizer;
    }

    public function normalize($object, string $format = null, array $context = []): array
    {
        $data = $this->normalizer->normalize($object, $format, $context);

        return [
            'msg' => $data['detail'] ?? 'validation failed',
            'status' => 'ERROR',
        ];
    }

    public function supportsNormalization($data, string $format = null): bool
    {
        return $data instanceof ConstraintViolationListInterface;
    }
}
