<?php

declare(strict_types=1);

namespace App\Api\EventListener;

use App\Api\Controller\V1\AbstractApiController;
use App\Api\DTO\Http\Response\ApiError;
use App\Api\Exception\ApiErrorExceptionInterface;
use App\Api\Exception\ConstraintViolationListException;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;
use Symfony\Component\Serializer\SerializerInterface;

class ApiExceptionListener
{
    private $serializer;
    private $logger;

    public function __construct(SerializerInterface $serializer, LoggerInterface $logger)
    {
        $this->serializer = $serializer;
        $this->logger = $logger;
    }

    public function onKernelException(ExceptionEvent $event)
    {
        $e = $event->getThrowable();
        if ($e instanceof ConstraintViolationListException) {
            $apiError = $e->getConstraintViolationList();
        } elseif ($e instanceof ApiErrorExceptionInterface) {
            $apiError = $e->getApiError();
        } else {
            if ($e instanceof HttpExceptionInterface) {
                $apiError = new ApiError($e->getMessage());
            } else {
                $apiError = new ApiError('internal server error');
            }
        }

        $isHandledException = $e instanceof HttpExceptionInterface;
        if ($isHandledException) {
            $statusCode = $e->getStatusCode();
            $headers = $e->getHeaders();
        } else {
            $this->logger->error('Not handled exception', ['exception' => $e]);
            $statusCode = 500;
            $headers = [];
        }

        $data = $this->serializer->serialize(
            $apiError,
            'xml',
            ['xml_root_node_name' => AbstractApiController::XML_ROOT_NODE_NAME,]
        );
        $response = new Response($data, $statusCode, $headers);
        $response->headers->set('Content-Type', 'text/xml');
        $event->setResponse($response);
    }
}
