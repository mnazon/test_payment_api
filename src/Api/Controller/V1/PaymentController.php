<?php

declare(strict_types=1);

namespace App\Api\Controller\V1;

use App\Api\DTO\Http\PaymentRequest;
use App\Api\Service\PaymentManager;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Swagger\Annotations as SWG;
use Nelmio\ApiDocBundle\Annotation\Model;

/**
 * @Route("/payments")
 *
 * @SWG\Tag(name="Payments")
 */
class PaymentController extends AbstractApiController
{
    /**
     * @Route("/credit", methods={"POST"})
     *
     * @SWG\Parameter(
     *     name="body",
     *     in="body",
     *     description="xml",
     *     required=true,
     *     format="application/xml",
     *     @Model(type=PaymentRequest::class),
     * ),
     *
     * @SWG\Response(
     *     response="200",
     *     description="request processed successfully",
     *     @SWG\Schema(ref="#/definitions/successResult"),
     * ),
     * @SWG\Response(
     *     response="400",
     *     description="validation error",
     *     @SWG\Schema(ref="#/definitions/constraintViolation"),
     * ),
     *
     * @var PaymentRequest $payment
     * @var PaymentManager $paymentManager
     *
     * @return Response
     */
    public function credit(PaymentRequest $payment, PaymentManager $paymentManager)
    {
        $paymentManager->credit($payment);

        return $this->xml('OK');
    }

    /**
     * @Route("/debit", methods={"POST"})
     *
     * @SWG\Parameter(
     *     name="body",
     *     in="body",
     *     description="xml",
     *     required=true,
     *     format="application/xml",
     *     @Model(type=PaymentRequest::class),
     * ),
     *
     * @SWG\Response(
     *     response="200",
     *     description="request processed successfully",
     *     @SWG\Schema(ref="#/definitions/successResult"),
     * ),
     ** @SWG\Response(
     *     response="400",
     *     description="validation error",
     *     @SWG\Schema(ref="#/definitions/constraintViolation"),
     * ),
     *
     * @param PaymentRequest $payment
     * @param PaymentManager $paymentManager
     *
     * @return Response
     */
    public function debit(PaymentRequest $payment, PaymentManager $paymentManager)
    {
        $paymentManager->debit($payment);

        return $this->xml('OK');
    }
}
