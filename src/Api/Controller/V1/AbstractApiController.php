<?php

declare(strict_types=1);

namespace App\Api\Controller\V1;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

abstract class AbstractApiController extends AbstractController
{
    public const XML_ROOT_NODE_NAME = 'result';

    private $defaultResponseContext = [
        'xml_root_node_name' => self::XML_ROOT_NODE_NAME,
    ];

    public function xml($data, $status = Response::HTTP_OK, array $context = [], array $headers = [])
    {
        $context = array_merge($this->defaultResponseContext, $context);
        $xml = $this->container->get('serializer')->serialize($data, 'xml', $context);

        $response = new Response($xml, $status, $headers);
        $response->headers->set('Content-Type', 'text/xml');

        return $response;
    }
}
