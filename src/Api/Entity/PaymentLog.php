<?php

namespace App\Api\Entity;

use App\Api\Enum\PaymentType;
use App\Api\Repository\PaymentLogRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=PaymentLogRepository::class)
 * @ORM\Table(name="payment_logs")
 */
class PaymentLog
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=10)
     */
    private $type;

    /**
     * @ORM\ManyToOne(targetEntity=User::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @ORM\Column(type="integer")
     */
    private $amount;

    /**
     * @ORM\Column(type="string", length=32, unique=true)
     */
    private $tid;

    /**
     * @ORM\Column(type="datetime_immutable")
     */
    private $createdAt;

    public function __construct(User $user, PaymentType $type, int $amount, string $tid)
    {
        $this->user = $user;
        $this->type = $type->getValue();
        $this->amount = $amount;
        $this->tid = $tid;
        $this->createdAt = new \DateTimeImmutable();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getAmount(): int
    {
        return $this->amount;
    }

    public function getCreatedAt(): \DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function getType(): PaymentType
    {
        return new PaymentType($this->type);
    }

    public function getUser(): User
    {
        return $this->user;
    }
}
