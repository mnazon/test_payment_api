<?php

declare(strict_types=1);

namespace App\Api\ArgumentResolver;

use App\Api\DTO\ArgumentResolvableInterface;
use App\Api\DTO\Http\Response\ApiError;
use App\Api\Exception\ApiErrorException;
use App\Api\Exception\ConstraintViolationListException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Controller\ArgumentValueResolverInterface;
use Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadata;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class BodyDataToDtoArgumentResolver implements ArgumentValueResolverInterface
{
    private $serializer;
    private $validator;

    public function __construct(ValidatorInterface $validator, SerializerInterface $serializer)
    {
        $this->serializer = $serializer;
        $this->validator = $validator;
    }

    public function supports(Request $request, ArgumentMetadata $argument)
    {
        return in_array($request->getMethod(), ['POST', 'PUT'])
            && class_exists($argument->getType())
            && in_array(ArgumentResolvableInterface::class, class_implements($argument->getType()), true)
        ;
    }

    public function resolve(Request $request, ArgumentMetadata $argument): iterable
    {
        $data = $request->getContent();
        try {
            $object = $this->serializer->deserialize($data, $argument->getType(), 'xml');
        } catch (\Throwable $e) {
            $apiError = new ApiError($e->getMessage());
            throw new ApiErrorException($apiError);
        }

        $violations = $this->validator->validate($object);
        if ($violations->count() > 0) {
            throw new ConstraintViolationListException($violations);
        }

        yield $object;
    }
}
