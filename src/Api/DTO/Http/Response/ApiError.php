<?php

declare(strict_types=1);

namespace App\Api\DTO\Http\Response;

class ApiError
{
    private $msg;
    private $status;

    public function __construct(string $msg, string $status = 'ERROR')
    {
        $this->msg = $msg;
        $this->status = $status;
    }

    public function getMsg(): string
    {
        return $this->msg;
    }

    public function getStatus(): string
    {
        return $this->status;
    }
}
