<?php

declare(strict_types=1);

namespace App\Api\DTO\Http;

use App\Api\DTO\ArgumentResolvableInterface;
use Symfony\Component\Validator\Constraints as Assert;

class PaymentRequest implements ArgumentResolvableInterface
{
    /**
     * @Assert\NotBlank()
     * @Assert\GreaterThan(0)
     * @var int
     */
    private $amount;
    /**
     * @Assert\NotBlank()
     * @Assert\Length(min="11", max="32")
     * @var string
     */
    private $tid;
    /**
     * @Assert\NotBlank()
     * @Assert\Length(min="1", max="32")
     * @var string
     */
    private $uid;

    public function __construct(int $amount = null, string $tid = null, string $uid = null)
    {
        $this->amount = $amount;
        $this->tid = $tid;
        $this->uid = $uid;
    }

    /**
     * @return int
     */
    public function getAmount(): int
    {
        return $this->amount;
    }

    /**
     * @return string
     */
    public function getTid(): string
    {
        return $this->tid;
    }

    /**
     * @return string
     */
    public function getUid(): string
    {
        return $this->uid;
    }
}
