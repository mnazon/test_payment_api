<?php

namespace App\Api\Repository;

use App\Api\Entity\PaymentLog;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method PaymentLog|null find($id, $lockMode = null, $lockVersion = null)
 * @method PaymentLog|null findOneBy(array $criteria, array $orderBy = null)
 * @method PaymentLog[]    findAll()
 * @method PaymentLog[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PaymentLogRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PaymentLog::class);
    }

    public function findOneByTransactionId(string $transactionId): ?PaymentLog
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.tid = :val')
            ->setParameter('val', $transactionId)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
}
