<?php

declare(strict_types=1);

namespace App\Api\Enum;

use MyCLabs\Enum\Enum;

/**
 * @method static CREDIT
 * @method static DEBIT
 */
class PaymentType extends Enum
{
    public const CREDIT = 'credit';
    public const DEBIT = 'debit';
}
