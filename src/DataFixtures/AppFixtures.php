<?php

namespace App\DataFixtures;

use App\Api\Entity\User;
use App\Api\Entity\Wallet;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $wallet = new Wallet(200);
        $user = new User('test0ebf671d414588792247984ctest', $wallet);
        $manager->persist($user);

        $manager->flush();
    }
}
